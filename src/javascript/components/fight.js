import { CRITICAL_HIT_SECS, MAX_RANDOM_VALUE, MIN_RANDOM_VALUE } from '../../constants/attacks';
import { controls } from '../../constants/controls';
import { getPercentage } from '../helpers/percentageHelper';
import { convertFighterToPlayer } from '../helpers/playerHelper';
import { getRandomBetween } from '../helpers/randomHelper';
import { getCurrentTime } from '../helpers/timeHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let firstPlayer = convertFighterToPlayer(firstFighter);
    let secondPlayer = convertFighterToPlayer(secondFighter);

    const firstIndicator = document.getElementById('left-fighter-indicator');
    const secondIndicator = document.getElementById('right-fighter-indicator');

    const keysPressed = new Set();

    document.body.onkeydown = (event) => {
      if (keysPressed.has(event.code)) return;
      keysPressed.add(event.code);

      firstPlayer, secondPlayer = performGameActions(
        keysPressed,
        firstPlayer,
        secondPlayer,
      );

      const firstIndicatorValue = getPercentage(firstPlayer.health, firstPlayer.maxHealth);
      const secondIndicatorValue = getPercentage(secondPlayer.health, secondPlayer.maxHealth);

      firstIndicator.style.width = `${firstIndicatorValue}%`;
      secondIndicator.style.width = `${secondIndicatorValue}%`;

      if (firstIndicatorValue === 0) {
        resolve(secondFighter);
      } else if (secondIndicatorValue === 0) {
        resolve(firstFighter);
      }
    };

    document.body.onkeyup = (event) => {
      keysPressed.delete(event.code);
    };
  });
}

function performGameActions(keysPressed, firstPlayer, secondPlayer) {
  firstPlayer, secondPlayer = performCriticalHits(keysPressed, firstPlayer, secondPlayer);
  firstPlayer, secondPlayer = performAttacks(keysPressed, firstPlayer, secondPlayer);

  return firstPlayer, secondPlayer;
}

function performAttacks(keysPressed, firstPlayer, secondPlayer) {
  firstPlayer, secondPlayer = maybeAttack({
    keysPressed,
    attackKey: controls.PlayerOneAttack,
    attackerBlockKey: controls.PlayerOneBlock,
    defenderBlockKey: controls.PlayerTwoBlock,
    attacker: firstPlayer,
    defender: secondPlayer,
  });

  secondPlayer, firstPlayer = maybeAttack({
    keysPressed,
    attackKey: controls.PlayerTwoAttack,
    attackerBlockKey: controls.PlayerTwoBlock,
    defenderBlockKey: controls.PlayerOneBlock,
    attacker: secondPlayer,
    defender: firstPlayer,
  });

  return firstPlayer, secondPlayer;
}

function performCriticalHits(keysPressed, firstPlayer, secondPlayer) {
  firstPlayer, secondPlayer = maybeCriticalHit({
    keysPressed,
    criticalCombo: controls.PlayerOneCriticalHitCombination,
    blockKey: controls.PlayerOneBlock,
    attacker: firstPlayer,
    defender: secondPlayer,
  });

  secondPlayer, firstPlayer = maybeCriticalHit({
    keysPressed,
    criticalCombo: controls.PlayerTwoCriticalHitCombination,
    blockKey: controls.PlayerTwoBlock,
    attacker: secondPlayer,
    defender: firstPlayer,
  });

  return firstPlayer, secondPlayer;
}

function maybeAttack({ keysPressed, attackKey, attackerBlockKey, defenderBlockKey, attacker, defender }) {
  const canAttack = keysPressed.has(attackKey);
  const attackerBlocking = keysPressed.has(attackerBlockKey);
  const defenderBlocking = keysPressed.has(defenderBlockKey);
  const blocking = attackerBlocking || defenderBlocking;

  if (canAttack && !blocking) {
    const damage = getDamage(attacker, defender);
    defender.health -= damage;
  }

  return attacker, defender;
}

function maybeCriticalHit({ keysPressed, criticalCombo, blockKey, attacker, defender }) {
  const currentTime = getCurrentTime();
  const blocking = keysPressed.has(blockKey);
  let canAttack = true;

  if (currentTime - attacker.lastCriticalAt < CRITICAL_HIT_SECS) {
    return attacker, defender;
  }

  for (const keyCode of criticalCombo) {
    canAttack = keysPressed.has(keyCode);
    if (!canAttack) break;
  }

  if (canAttack && !blocking) {
    const damage = MAX_RANDOM_VALUE * attacker.attack;
    attacker.lastCriticalAt = getCurrentTime();
    defender.health -= damage;
  }

  return attacker, defender;
}

export function getDamage(attacker, defender) {
  const result = getHitPower(attacker) - getBlockPower(defender);
  const minTreshold = Math.max(result, 0);

  return minTreshold;
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomBetween(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomBetween(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE);
  return fighter.defense * dodgeChance;
}

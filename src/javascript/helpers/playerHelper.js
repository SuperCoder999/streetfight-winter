import { CRITICAL_HIT_SECS } from '../../constants/attacks';
import { getCurrentTime } from './timeHelper';

export function convertFighterToPlayer(fighter) {
  return {
    ...fighter,
    maxHealth: fighter.health,
    lastCriticalAt: getCurrentTime() - CRITICAL_HIT_SECS,
  };
}

import { RELOAD_GAME_TIMEOUT_MS } from '../../../constants/time';
import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const bodyElement = createElement({
    tagName: 'div',
    className: 'modal-body',
  });

  bodyElement.innerText = `${fighter.name} wins!`;

  const onClose = () => {
    setTimeout(() => window.location.reload(), RELOAD_GAME_TIMEOUT_MS);
  };

  showModal({
    title: 'Battle is over!',
    bodyElement,
    onClose,
  });
}

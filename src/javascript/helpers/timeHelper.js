import { MILISECONDS_IN_SECOND } from "../../constants/time";

export function getCurrentTime() {
  return performance.now() / MILISECONDS_IN_SECOND;
}

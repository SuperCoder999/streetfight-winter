import { RANDOM_ADD_DIVIDER } from "../../constants/random";

export function getRandomBetween(min, max) {
  const multiplier = max - min;
  const random = Math.random();
  const addRandom = Math.random() / RANDOM_ADD_DIVIDER;
  const result = min + random * multiplier + addRandom;
  const maxTreshold = Math.min(result, max);

  return maxTreshold;
}

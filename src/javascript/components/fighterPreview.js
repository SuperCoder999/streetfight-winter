import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';

  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const paramsElement = createFighterParamsPreview(fighter);
    const imageElement = createFighterImage(fighter);
    fighterElement.append(paramsElement, imageElement);
  }

  return fighterElement;
}

function createFighterParamsPreview(fighter) {
  const paramsElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___parameter-root',
  });

  const nameElement = createFighterParameterPreview({
    value: fighter.name,
    isHead: true
  });

  const healthElement = createFighterParameterPreview({
    name: 'Health',
    value: fighter.health,
  });

  const attackElement = createFighterParameterPreview({
    name: 'Attack',
    value: fighter.attack,
  });

  const defenseElement = createFighterParameterPreview({
    name: 'Defense',
    value: fighter.defense,
  });

  paramsElement.append(nameElement, healthElement, attackElement, defenseElement);
  return paramsElement;
}

function createFighterParameterPreview({ value, name = null, isHead = false }) {
  const headClassName = isHead ? 'fighter-preview___parameter-head' : '';

  const element = createElement({
    tagName: 'div',
    className: `fighter-preview___parameter ${headClassName}`,
  });

  const textStart = name ? `${name}: ` : '';
  element.innerText = `${textStart}${value}`;

  return element;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;

  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };

  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function getPercentage(current, max) {
    return Math.max(current / max * 100, 0);
}